<?php

namespace Drupal\pinterest_hover\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;

/**
 * Class ConfigForm implementing the configuration form.
 *
 * @package Drupal\pinterest_hover\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pinterest_hover.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pinterest_hover_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pinterest_hover.settings');
    $form['load_pinterest_js'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add Pinterest script tag to all pages'),
      '#description' => $this->t("Whether this module should add a script tag to all pages loading Pinterest's JavaScript and enabling hover buttons. You may want to turn this off if you are including the Pinterest JavaScript in another way, such as in a different module or theme."),
      '#default_value' => $config->get('load_pinterest_js'),
    ];
    $form['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size'),
      '#description' => $this->t('Select the size of the pin hover button.'),
      '#default_value' => !empty($config->get('size')) ? $config->get('size') : 0,
      '#empty_option' => $this->t('- Select -'),
      '#options' => [
        0 => $this->t('Small'),
        28 => $this->t('Large'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="load_pinterest_js"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['shape'] = [
      '#type' => 'select',
      '#title' => $this->t('Shape'),
      '#description' => $this->t('Select the shape of the pin hover button. Will default to rectangular.'),
      '#default_value' => !empty($config->get('shape')) ? $config->get('shape') : 'rect',
      '#empty_option' => $this->t('- Select -'),
      '#options' => [
        'rect' => $this->t('Rectangular'),
        'round' => $this->t('Circular'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="load_pinterest_js"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['color'] = [
      '#type' => 'select',
      '#title' => $this->t('Color'),
      '#description' => $this->t('Select the color of the pin hover button. Only affects the rectangular shape.'),
      '#default_value' => !empty($config->get('color')) ? $config->get('color') : 'grey',
      '#empty_option' => $this->t('- Select -'),
      '#options' => [
        'gray' => $this->t('Gray'),
        'red' => $this->t('Red'),
        'white' => $this->t('White'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="load_pinterest_js"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $options = [];
    try {
      $content_types = $this->entityTypeManager->getStorage('node_type')
        ->loadMultiple();
      foreach ($content_types as $machine_name => $type) {
        $options[$machine_name] = $type->get('name');
      }
    }
    catch (PluginNotFoundException | InvalidPluginDefinitionException $exception) {
      watchdog_exception('pinterest_hover', $exception);
    }
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Types'),
      '#description' => $this->t('If content types are selected, script tag will be only added to these content types. If none are selected, all pages will have script added.'),
      '#default_value' => !empty($config->get('content_types')) ? $config->get('content_types') : [],
      '#options' => $options,
    ];
    $form['exclude_hover_selectors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Excluded items based on CSS selectors'),
      '#description' => $this->t('CSS selectors matching images that should not be pinnable or have Pin It hover buttons. One selector per line. Tip: to exclude all images in an area of your page, use descendant selectors like ".sidebar img"'),
      '#default_value' => $config->get('exclude_hover_selectors'),
      '#states' => [
        'visible' => [
          ':input[name="load_pinterest_js"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Normalize line endings to \n.
    $selectors = preg_replace('/[\r\n]+/', "\n", $form_state->getValue('exclude_hover_selectors'));

    $this->config('pinterest_hover.settings')
      ->set('load_pinterest_js', $form_state->getValue('load_pinterest_js'))
      ->set('exclude_hover_selectors', $selectors)
      ->set('size', $form_state->getValue('size'))
      ->set('shape', $form_state->getValue('shape'))
      ->set('color', $form_state->getValue('color'))
      ->set('content_types', $form_state->getValue('content_types'))
      ->save();

    // Invalidate cache tags.
    Cache::invalidateTags(['pinterest_hover']);
  }

}
