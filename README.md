CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

A simple module to allow the Pinterest On Hover Pin It button to be added for
images on the site.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/pinterest_hover

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.

Configuration UI is available to exclude images from displaying the Pin It
button.


MAINTAINERS
-----------

Current maintainers:
* 9.x: George Anderson (geoanders) - https://www.drupal.org/u/geoanders
* 8.x: Mike Baynton (mbaynton) - https://www.drupal.org/u/mbaynton
* 7.x: Joshua Stewardson (sokrplare) - https://www.drupal.org/u/sokrplare

This project has been sponsored by:
 * Meredith Publishing.
   http://www.meredith.com
